import java.awt.EventQueue;
import javax.swing.JFrame;


public class Ants extends JFrame {

    public Ants() {

        add(new Board());
        
        setResizable(false);
        pack();
        
        setTitle("Finding Beer");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {                
                JFrame ex = new Ants();
                ex.setVisible(true);                
            }
        });
    }
}