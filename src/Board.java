import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Line2D;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JPanel implements ActionListener {

	private final int ANT_COUNT = 3;
	private final int RESOURCE_COUNT = 10;
	private final int RESOURCE_QUANTITY = 10;
	private final int B_WIDTH = 600;
	private final int B_HEIGHT = 600;
	private final int DOT_SIZE = 50;
	private final int RAND_POS = Math.min(B_WIDTH, B_HEIGHT) / DOT_SIZE;
	private final int DELAY = 200;

	private int base_x;
	private int base_y;

	private final int paths_on_board_matrix[][] = new int[B_WIDTH / DOT_SIZE][B_HEIGHT / DOT_SIZE];

	private LinkedList<Resource> resources = new LinkedList<Resource>();
	private LinkedList<Ant> ants = new LinkedList<Ant>();

	private Timer timer;
	private Image base_ico;
	private Image resource_ico;
	private Image ant_ico;

	public Board() {

		setBackground(Color.black);
		setFocusable(true);

		setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
		loadImages();
		initGame();
	}

	private void loadImages() {

		ImageIcon iid = new ImageIcon("icons/base.png");
		base_ico = iid.getImage();

		ImageIcon iia = new ImageIcon("icons/resource.png");
		resource_ico = iia.getImage();

		ImageIcon iih = new ImageIcon("icons/ant.png");
		ant_ico = iih.getImage();
	}

	private void initGame() {

		for (int i = 0; i < paths_on_board_matrix.length; i++) {
			for (int j = 0; j < paths_on_board_matrix[0].length; j++) {
				paths_on_board_matrix[i][j] = 0;
			}
		}

		for (int i = 0; i < RESOURCE_COUNT; i++) {
			resources.add(new Resource(getRandCoord(), getRandCoord(), RESOURCE_QUANTITY));
		}

		this.base_x = getRandCoord();
		this.base_y = getRandCoord();

		for (int i = 0; i < ANT_COUNT; i++) {
			ants.add(new Ant(i + 1, base_x, base_y));
		}

		timer = new Timer(DELAY, this);
		timer.start();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		doDrawing(g);
	}

	private void doDrawing(Graphics g) {

		// drawing paths
		for (int i = 0; i < paths_on_board_matrix.length; i++) {
			for (int j = 0; j < paths_on_board_matrix[0].length; j++) {
				if (paths_on_board_matrix[i][j] != 0) {
					g.setColor(Color.DARK_GRAY);
					g.fillRect(i * DOT_SIZE, j * DOT_SIZE, DOT_SIZE, DOT_SIZE);
				}
			}
		}

		// drawing net
		g.setColor(Color.GRAY);
		for (int i = DOT_SIZE; i < B_WIDTH; i += DOT_SIZE) {
			g.drawLine(i, 0, i, B_HEIGHT);
			g.drawLine(0, i, B_WIDTH, i);
		}

		// drawing base
		g.drawImage(base_ico, base_x, base_y, this);

		// drawing resources
		Font small = new Font("Helvetica", Font.BOLD, 14);
		g.setColor(Color.white);
		g.setFont(small);

		for (Resource resource : resources) {
			if (resource.resource_cnt != 0) {
				g.drawImage(resource_ico, resource.x_coord, resource.y_coord, this);
				g.drawString(String.valueOf(resource.resource_cnt), resource.x_coord, resource.y_coord + DOT_SIZE);
			}
		}

		// drawing ants
		for (Ant ant : ants) {
			g.drawImage(ant_ico, ant.x_coord, ant.y_coord, this);
			if (ant.isHoldingResource()) {
				g.drawImage(resource_ico, ant.x_coord, ant.y_coord, this);
			}
		}

		Toolkit.getDefaultToolkit().sync();

	}

	private void checkResource(Ant ant) {
		for (Resource resource : resources) {
			if (ant.x_coord == resource.x_coord && ant.y_coord == resource.y_coord) {
				if (ant.isSearching() && resource.resource_cnt != 0) {
					ant.setSearching(false);
					ant.setMakingPath(true);
					ant.resource.x = resource.x_coord;
					ant.resource.y = resource.y_coord;
					resource.resource_cnt = resource.resource_cnt - 1;
					ant.setHoldingResource(true);
				}
				if (ant.isWorking() && (ant.resource.x == resource.x_coord && ant.resource.y == resource.y_coord)) {
					if (resource.resource_cnt == 0) {
						ant.setWorking(false);
						ant.setSearching(true);
						ant.setHoldingLastResource(false);
						for (Node n : ant.path) {
							if (paths_on_board_matrix[n.x / DOT_SIZE][n.y / DOT_SIZE] == ant.id) {
								paths_on_board_matrix[n.x / DOT_SIZE][n.y / DOT_SIZE] = 0;
							}
						}
						ant.path.clear();
					} else {
						if (resource.resource_cnt == 1) {
							ant.setHoldingLastResource(true);
						}
						resource.resource_cnt = resource.resource_cnt - 1;
						ant.setHoldingResource(true);
					}

				}
			}
		}
	}

	private void checkPath(Ant ant) {
		int cell = paths_on_board_matrix[ant.x_coord / DOT_SIZE][ant.y_coord / DOT_SIZE];
		if (cell != 0) {
			ant.resource.x = ants.get(cell - 1).resource.x;
			ant.resource.y = ants.get(cell - 1).resource.y;
			ant.path = new LinkedList<Node>(ants.get(cell - 1).path);
			ant.path_pos = ant.path.indexOf(new Node(ant.x_coord, ant.y_coord));
			ant.setSearching(false);
			ant.setWorking(true);
		}
	}

	private void checkBase(Ant ant) {
		if (ant.x_coord == base_x && ant.y_coord == base_y) {
			ant.setHoldingResource(false);
			if (ant.isHoldingLastResource()) {
				ant.setWorking(false);
				ant.setSearching(true);
				ant.setHoldingLastResource(false);
				for (Node n : ant.path) {
					if (paths_on_board_matrix[n.x / DOT_SIZE][n.y / DOT_SIZE] == ant.id) {
						paths_on_board_matrix[n.x / DOT_SIZE][n.y / DOT_SIZE] = 0;
					}
				}
				ant.path.clear();
			}
		}
	}

	private int getRandCoord() {

		int r = (int) (Math.random() * RAND_POS);
		return ((r * DOT_SIZE));

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		for (Ant ant : ants) {
			if (ant.isSearching()) {
				checkPath(ant);
				ant.search();
				checkResource(ant);
			} else if (ant.isMakingPath()) {
				ant.makePath(base_x, base_y);
			} else if (ant.isWorking()) {
				ant.followPath();
				checkResource(ant);
				checkBase(ant);
			}

		}

		repaint();
	}

	private class Resource {
		private int x_coord;
		private int y_coord;
		private int resource_cnt;

		public Resource(int x, int y, int resource_cnt) {
			this.x_coord = x;
			this.y_coord = y;
			this.resource_cnt = resource_cnt;
		}
	}

	private class Ant {

		private int id;
		private int x_coord;
		private int y_coord;
		private Node resource = new Node(0, 0);
		private List<Node> path;
		private boolean working;
		private boolean makingPath;
		private boolean searching;
		private boolean holdingResource;
		private boolean holdingLastResource;
		private int path_pos;

		public Ant(int id, int x, int y) {
			this.id = id;
			this.x_coord = x;
			this.y_coord = y;
			this.path = new LinkedList<Node>();
			this.working = false;
			this.holdingResource = false;
			this.holdingLastResource = false;
			this.searching = true;
			this.makingPath = false;
		}

		private void makePath(int x, int y) {

			paths_on_board_matrix[this.x_coord / DOT_SIZE][this.y_coord / DOT_SIZE] = this.id;
			this.path.add(new Node(this.x_coord, this.y_coord));
			
			if (this.x_coord == x && this.y_coord == y) {
				this.setWorking(true);
				this.setHoldingResource(false);
				this.setMakingPath(false);

				// making path from base to resource and back to base
				LinkedList<Node> p = new LinkedList<Node>(path);
				p.removeFirst();
				Collections.reverse(path);
				path.addAll(p);
				this.path_pos = path.size() - 1;
			}
			// move left
			if (this.x_coord > x) {
				this.x_coord = this.x_coord - DOT_SIZE;
			}
			// move right
			else if (this.x_coord < x) {
				this.x_coord = this.x_coord + DOT_SIZE;
			}
			// move up
			else if (this.y_coord > y) {
				this.y_coord = this.y_coord - DOT_SIZE;
			}
			// move down
			else if (this.y_coord < y) {
				this.y_coord = this.y_coord + DOT_SIZE;
			}

		}

		private void followPath() {
			if (this.path_pos == 0) {
				this.path_pos = path.size();
			}
			this.path_pos = this.path_pos - 1;
			Node node = path.get(this.path_pos);
			this.x_coord = node.x;
			this.y_coord = node.y;
		}

		private void search() {

			int r = (int) (Math.random() * 100);
			// move left
			if ((r > 0 && r <= 25) && (this.x_coord != 0)) {
				this.x_coord = this.x_coord - DOT_SIZE;
			}
			// move right
			if ((r > 25 && r <= 50) && ((this.x_coord != B_WIDTH - DOT_SIZE))) {
				this.x_coord = this.x_coord + DOT_SIZE;
			}
			// move up
			if ((r > 50 && r <= 75) && (this.y_coord != 0)) {
				this.y_coord = this.y_coord - DOT_SIZE;
			}
			// move down
			if ((r > 75 && r <= 100) && (this.y_coord != B_HEIGHT - DOT_SIZE)) {
				this.y_coord = this.y_coord + DOT_SIZE;
			}

		}

		public boolean isWorking() {
			return working;
		}

		public void setWorking(boolean working) {
			this.working = working;
		}

		public boolean isSearching() {
			return searching;
		}

		public void setSearching(boolean searching) {
			this.searching = searching;
		}

		public boolean isHoldingResource() {
			return holdingResource;
		}

		public void setHoldingResource(boolean holdingResource) {
			this.holdingResource = holdingResource;
		}

		public boolean isMakingPath() {
			return makingPath;
		}

		public void setMakingPath(boolean makingPath) {
			this.makingPath = makingPath;
		}

		public boolean isHoldingLastResource() {
			return holdingLastResource;
		}

		public void setHoldingLastResource(boolean holdingLastResource) {
			this.holdingLastResource = holdingLastResource;
		}
	}

	private class Node {
		public int x;
		public int y;

		public Node(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public boolean equals(Object other) {
			if (other == null)
				return false;
			if (other == this)
				return true;
			if (!(other instanceof Node))
				return false;
			Node node = (Node) other;
			if (x == node.x && y == node.y)
				return true;
			else
				return false;
		}
	}
}